import * as THREE from "three";
import Prefab from "./Prefab";

export default class Camera {
  constructor() {
    this.prefab = new Prefab();
    this.sizes = this.prefab.sizes;
    this.scene = this.prefab.scene;
    this.canvas = this.prefab.canvas;

    this.setInstance();
  }

  setInstance() {
    this.camera = new THREE.PerspectiveCamera(
      75,
      this.sizes.width / this.sizes.height,
      0.1,
      1000
    );
    this.camera.position.set(0, 1, 4);
    this.scene.add(this.camera);
  }

  setControls() {
    this.controls = new OrbitControls(this.camera, this.canvas);
    this.controls.enableDamping = true;
  }

  resize() {
    this.camera.aspect = this.sizes.width / this.sizes.height;
    this.camera.updateProjectionMatrix();
  }
}

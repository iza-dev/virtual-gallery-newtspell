import * as THREE from "three";
import Prefab from "./Prefab.js";

export default class Renderer {
  constructor() {
    this.prefab = new Prefab();
    this.canvas = this.prefab.canvas;
    this.sizes = this.prefab.sizes;
    this.scene = this.prefab.scene;
    this.camera = this.prefab.camera;

    this.setInstance();
  }

  setInstance() {
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas,
      antialias: true,
    });
    this.renderer.autoClear = true;
    this.renderer.physicallyCorrectLights = true;
    this.renderer.outputEncoding = THREE.sRGBEncoding;
    this.renderer.toneMapping = THREE.CineonToneMapping;
    this.renderer.toneMappingExposure = 1.75;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    this.renderer.setClearColor("#2e0a4f");
    this.renderer.setSize(this.sizes.width, this.sizes.height);
    this.renderer.setPixelRatio(Math.min(this.sizes.pixelRatio, 2));
  }

  resize() {
    this.renderer.setSize(this.sizes.width, this.sizes.height);
    this.renderer.setPixelRatio(Math.min(this.sizes.pixelRatio, 2));
  }

  update() {
    this.renderer.render(this.scene, this.camera.camera);
  }
}

import * as THREE from "three";
import Prefab from "../Prefab";
import { JoyStick } from "./JoyStick.js";

export default class Controller {
  constructor(world) {
    this.prefab = new Prefab();
    this.clock = new THREE.Clock();
    this.camera = this.prefab.camera.camera;
    this.delta = 0.001 * this.prefab.time.delta;
    this.target = world.player.wrapperHoverboard;

    this.raycaster = new THREE.Raycaster();

    this.move = { up: 0, right: 0 };
    this.look = { up: 0, right: 0 };

    this.tmpVec3 = new THREE.Vector3();
    this.tmpQuat = new THREE.Quaternion();

    this.cameraBase = new THREE.Object3D();
    this.cameraBase.position.copy(this.camera.position);
    this.cameraBase.quaternion.copy(this.camera.quaternion);
    this.target.attach(this.cameraBase);

    this.cameraHigh = new THREE.Camera();
    this.cameraHigh.position.copy(this.camera.position);
    this.cameraHigh.position.y += 10;
    this.cameraHigh.lookAt(this.target.position);
    this.target.attach(this.cameraHigh);

    this.yAxis = new THREE.Vector3(0, 1, 0);
    this.xAxis = new THREE.Vector3(1, 0, 0);
    this.forward = new THREE.Vector3(0, 0, 1);
    this.down = new THREE.Vector3(0, -1, 0);

    this.speed = 5;

    this.checkForGamepad();

    if ("ontouchstart" in document.documentElement) {
      this.initOnScreenController();
    } else {
      this.initKeyboardControll();
    }
  }

  initOnScreenController() {
    const options1 = {
      left: true,
      app: this,
      onMove: this.onMove,
    };
    const joystick1 = new JoyStick(options1);

    this.touchController = { joystick1 };
  }

  checkForGamepad() {
    const gamepads = {};
    const self = this;

    function gamepadConnect(event, connecting) {
      const gamepad = event.gamepad;

      if (connecting) {
        gamepads[gamepad.index] = gamepad;
        self.gamepad = gamepad;
        if (self.touchController) self.showTouchController(false);
      } else {
        delete self.gamepad;
        delete gamepads[gamepad.index];
        if (self.touchController) self.showTouchController(true);
      }
    }

    window.addEventListener(
      "gamepadconnected",
      function (e) {
        gamepadConnect(e, true);
      },
      false
    );
    window.addEventListener(
      "gamepaddisconnected",
      function (e) {
        gamepadConnect(e, false);
      },
      false
    );
  }

  showTouchController(mode) {
    if (this.touchController == undefined) return;

    this.touchController.joystick1.visible = mode;
    this.touchController.joystick2.visible = mode;
  }

  initKeyboardControll() {
    //38 => up key
    //40 => down key
    //37=> left key
    //39 => right key
    document.addEventListener("keydown", this.keyDown.bind(this));
    document.addEventListener("keyup", this.keyUp.bind(this));
    this.keys = {
      up: false,
      down: false,
      left: false,
      right: false,
    };
  }

  gamepadHandler() {
    const gamepads = navigator.getGamepads();
    const gamepad = gamepads[this.gamepad.index];
    const leftStickX = gamepad.axes[0];
    const leftStickY = gamepad.axes[1];
    const rightStickX = gamepad.axes[2];
    const rightStickY = gamepad.axes[3];
    this.onMove(-leftStickY, leftStickX);
    this.onMove(-rightStickY, rightStickX);
  }

  keyDown(e) {
    switch (e.keyCode) {
      case 38:
        this.keys.up = true;
        break;
      case 40:
        this.keys.down = true;
        break;
      case 37:
        this.keys.left = true;
        break;
      case 39:
        this.keys.right = true;
        break;
    }
  }

  keyUp(e) {
    switch (e.keyCode) {
      case 38:
        this.keys.up = false;
        if (!this.keys.down) this.move.up = 0;
        break;
      case 40:
        this.keys.down = false;
        if (!this.keys.up) this.move.up = 0;
        break;
      case 37:
        this.keys.left = false;
        if (!this.keys.right) this.move.right = 0;
        break;
      case 39:
        this.keys.right = false;
        if (!this.keys.left) this.move.right = 0;
        break;
    }
  }

  onMove(up, right) {
    this.move.up = up;
    this.move.right = -right;
  }

  onLook(up, right) {
    this.look.up = up * 0.25;
    this.look.right = -right;
  }

  keyHandler() {
    if (this.keys.up) this.move.up += 0.1;
    if (this.keys.down) this.move.up -= 0.1;
    if (this.keys.left) this.move.right += 0.1;
    if (this.keys.right) this.move.right -= 0.1;

    if (this.move.up > 1) this.move.up = 1;
    if (this.move.up < -1) this.move.up = -1;
    if (this.move.right > 1) this.move.right = 1;
    if (this.move.right < -1) this.move.right = -1;
  }

  update() {
    let playerMoved = false;
    let speed;

    if (this.gamepad) {
      this.gamepadHandler();
    } else if (this.keys) {
      this.keyHandler();
    }

    if (this.move.up > 0) this.target.position.z -= 0.2;

    if (this.move.up < 0) this.target.position.z += 0.5;

    if (Math.abs(this.move.right) > 0.1) {
      const theta = this.delta * (this.move.right - 0.1) * 1;
      this.target.rotateY(theta);
      playerMoved = true;
    }

    if (playerMoved) {
      this.cameraBase.getWorldPosition(this.tmpVec3);
      this.camera.position.lerp(this.tmpVec3, 0.7);
      if (speed > 0.03) {
        if (this.overRunSpeedTime) {
          const elapsedTime = this.clock.elapsedTime - this.overRunSpeedTime;
        } else {
          this.overRunSpeedTime = this.clock.elapsedTime;
        }
      } else {
        delete this.overRunSpeedTime;
      }
    }

    if (this.look.up === 0 && this.look.right === 0) {
      let lerpSpeed = 0.7;
      this.cameraBase.getWorldPosition(this.tmpVec3);
      this.cameraBase.getWorldQuaternion(this.tmpQuat);
      this.camera.position.lerp(this.tmpVec3, lerpSpeed);
      this.camera.quaternion.slerp(this.tmpQuat, lerpSpeed);
    } else {
      this.camera.rotateOnWorldAxis(this.yAxis, this.look.right * this.delta);
      const cameraXAxis = this.xAxis
        .clone()
        .applyQuaternion(this.camera.quaternion);
      this.camera.rotateOnWorldAxis(cameraXAxis, this.look.up * this.delta);
    }
  }
}

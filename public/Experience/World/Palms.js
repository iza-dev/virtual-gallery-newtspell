import * as THREE from "three";
import { mergeBufferGeometries } from "three/examples/jsm/utils/BufferGeometryUtils";
import Prefab from "../Prefab";

export default class Palms {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;

    this.time = 0;
    this.clock = new THREE.Clock();

    this.setUp();
  }

  setUp() {
    this.materialShaders = [];
    this.speed = 10;
    this.palmGeoms = [];

    this.logGeom = new THREE.CylinderBufferGeometry(
      0.25,
      0.125,
      10,
      5,
      4,
      true
    );
    this.logGeom.translate(0, 5, 0);
    this.palmGeoms.push(this.logGeom);

    for (let i = 0; i < 20; i++) {
      this.leafGeom = new THREE.CircleBufferGeometry(1.25, 4);
      this.leafGeom.translate(0, 1.25, 0);
      this.leafGeom.rotateX(-Math.PI * 0.5);
      this.leafGeom.scale(0.25, 1, THREE.Math.randFloat(1, 1.5));
      this.leafGeom.attributes.position.setY(0, 0.25);
      this.leafGeom.rotateX(THREE.Math.randFloatSpread(Math.PI * 0.5));
      this.leafGeom.rotateY(THREE.Math.randFloat(0, Math.PI * 2));
      this.leafGeom.translate(0, 10, 0);
      this.palmGeoms.push(this.leafGeom);
    }
    this.palmGeom = mergeBufferGeometries(this.palmGeoms, false);
    this.palmGeom.rotateZ(THREE.Math.degToRad(-1.5));

    this.instPalm = new THREE.InstancedBufferGeometry();
    this.instPalm.attributes.position = this.palmGeom.attributes.position;
    this.instPalm.attributes.uv = this.palmGeom.attributes.uv;
    this.instPalm.index = this.palmGeom.index;
    this.palmPos = [];
    for (let i = 0; i < 5; i++) {
      this.palmPos.push(-5, 0, i * 20 - 10 - 50);
      this.palmPos.push(5, 0, i * 20 - 50);
    }
    this.instPalm.addAttribute(
      "instPosition",
      new THREE.InstancedBufferAttribute(new Float32Array(this.palmPos), 3)
    );

    this.palmMat = new THREE.MeshBasicMaterial({
      color: 0x00ff88,
      side: THREE.DoubleSide,
    });
    this.palmMat.onBeforeCompile = (shader) => {
      shader.uniforms.time = { value: 0 };
      shader.vertexShader =
        `
    uniform float time;
    attribute vec3 instPosition;
  ` + shader.vertexShader;
      shader.vertexShader = shader.vertexShader.replace(
        `#include <begin_vertex>`,
        `#include <begin_vertex>

      transformed.x *= sign(instPosition.x); // flip
      vec3 ip = instPosition;
      ip.z = mod(50. + ip.z + time * ${this.speed}., 100.) - 50.;
      transformed *= 0.4 + smoothstep(50., 45., abs(ip.z)) * 0.6;
      transformed += ip;
    `
      );
      this.materialShaders.push(shader);
    };
    this.palms = new THREE.Mesh(this.instPalm, this.palmMat);
    this.scene.add(this.palms);
  }

  update() {
    this.time = this.clock.getElapsedTime();
    this.materialShaders.forEach((m) => {
      m.uniforms.time.value = this.time;
    });
  }
}

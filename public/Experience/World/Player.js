import * as THREE from "three";
import Prefab from "../Prefab";

export default class Player {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;
    this.resource = this.resources.items.black_girl_hoverboard_model;
    this.camera = this.prefab.camera.camera;

    //create a wrapper object for hoverboard
    this.wrapperHoverboard = new THREE.Object3D();
    this.wrapperHoverboard.name = "controlHoverboard";

    this.setModel();
  }

  setModel() {
    this.model = this.resource.scene;
    this.scene.add(this.model);

    this.model.traverse((child) => {
      if (child.name === "hoverboard") {
        this.hoverboard = child;
        //wrapping hoverboard for fix problem orientation of model (from blender)
        this.wrapperHoverboard.add(this.hoverboard);
        this.wrapperHoverboard.rotation.y = Math.PI * 1;

        this.scene.add(this.wrapperHoverboard);
      }
      if (child instanceof THREE.Mesh) child.castShadow = true;
    });
  }
}

import * as THREE from "three";
import Prefab from "../Prefab";
import { noise } from "../shaders/road/noise";
import fragmentRoad from "../shaders/road/fragment";

export default class Road {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;

    this.time = 0;
    this.clock = new THREE.Clock();

    this.setUp();
  }

  setUp() {
    this.speed = 10;
    this.materialShaders = [];
    this.roadGeometry = new THREE.PlaneBufferGeometry(100, 100, 200, 200);
    this.roadGeometry.rotateX(-Math.PI * 0.5);
    this.roadMaterial = new THREE.MeshBasicMaterial({ color: 0xff00ee });
    this.roadMaterial.extensionDerivatives = true;
    this.roadMaterial.onBeforeCompile = (shader) => {
      shader.uniforms.time = { value: 0 };
      shader.vertexShader =
        `
        uniform float time;
        uniform bool uDirection;
        varying vec3 vPos;
      ` +
        noise +
        shader.vertexShader;
      shader.vertexShader = shader.vertexShader.replace(
        `#include <begin_vertex>`,
        `#include <begin_vertex>
                vec2 tuv = uv;
                float t = time * 0.01 * ${this.speed}.;
                tuv.y += t;
                transformed.y = snoise(vec3(tuv * 5., 0.)) * 5.;
                transformed.y *= smoothstep(5., 15., abs(transformed.x)); // road stripe
                vPos = transformed;
              `
      );
      shader.fragmentShader = fragmentRoad;
      shader.extensionDerivatives = true;
      this.materialShaders.push(shader);
    };
    this.setMesh();
  }

  setMesh() {
    this.road = new THREE.Mesh(this.roadGeometry, this.roadMaterial);
    this.scene.add(this.road);
  }

  update() {
    this.time = this.clock.getElapsedTime();
    this.materialShaders.forEach((m) => {
      m.uniforms.time.value = this.time;
    });
  }
}

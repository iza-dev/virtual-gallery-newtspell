import * as THREE from "three";
import Prefab from "../Prefab";
import fragmentSun from "../shaders/sun/fragment";
import vertexSun from "../shaders/sun/vertex";

export default class Sun {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;
    this.time = 0;
    this.clock = new THREE.Clock();

    this.uniforms = {
      uTime: { value: 0 },
      iResolution: { value: new THREE.Vector3() },
    };

    this.setUp();
  }

  setUp() {
    this.sunGeometry = new THREE.PlaneBufferGeometry(500, 500, 200, 200);
    this.sunMaterial = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader: vertexSun,
      fragmentShader: fragmentSun,
    });
    this.sun = new THREE.Mesh(this.sunGeometry, this.sunMaterial);
    this.sun.frustumCulled = true;
    this.sun.position.set(0, 100, -600);
    this.scene.add(this.sun);
  }

  update() {
    this.time = this.clock.getElapsedTime();
    this.uniforms.uTime.value = this.time;
  }
}

import Controller from "../Utils/Controller";
import Prefab from "../Prefab.js";
import Environment from "./Environment.js";
import Player from "./Player.js";
import Road from "./Road.js";
import Palms from "./Palms.js";
import Sun from "./Sun.js";
export default class World {
  constructor() {
    this.prefab = new Prefab();
    this.scene = this.prefab.scene;
    this.resources = this.prefab.resources;

    this.resources.on("ready", () => {
      this.road = new Road();
      this.palms = new Palms();
      this.sun = new Sun();
      this.player = new Player();
      this.environment = new Environment();
      this.controller = new Controller(this);
    });
  }

  update() {
    if (this.road) this.road.update();
    if (this.sun) this.sun.update();
    if (this.palms) this.palms.update();
    if (this.controller) this.controller.update();
  }
}

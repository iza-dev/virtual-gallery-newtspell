const fragmentRoad = `
    uniform vec3 diffuse;
    uniform float opacity;
    uniform float time;
    varying vec3 vPos;

    float line(vec3 position, float width, vec3 step){
    vec3 tempCoord = position / step;
    
    vec2 coord = tempCoord.xz;
    coord.y -= time * 10. / 2.;

    vec2 grid = abs(fract(coord - 0.5) - 0.5) / fwidth(coord * width);
    float line = min(grid.x, grid.y);
    
    return min(line, 1.0);
    }
    void main() {
        vec3 outgoingLight = vec3(0.0);
        vec4 diffuseColor = vec4(diffuse, opacity);
        float l = line(vPos, 2.0, vec3(2.0));
        vec3 base = mix(vec3(0, 0.75, 1), vec3(0), smoothstep(5., 7.5, abs(vPos.x)));
        vec3 c = mix(outgoingLight, base, l);
        gl_FragColor = vec4(c, diffuseColor.a);
    }
`;

export default fragmentRoad;

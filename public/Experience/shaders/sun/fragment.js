const shadertoyBoilerplate = `
float sun(vec2 uv, float battery)
{
 	float val = smoothstep(0.3, 0.29, length(uv));
 	float bloom = smoothstep(0.7, 0.0, length(uv));
    float cut = 3.0 * sin((uv.y + uTime * 0.2 * (battery + 0.02)) * 100.0) 
				+ clamp(uv.y * 14.0 + 1.0, -6.0, 6.0);
    cut = clamp(cut, 0.0, 1.0);
    return clamp(val * cut, 0.0, 1.0) + bloom * 0.1;
}
void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
   //vec2 uv = (1.65 * fragCoord.xy - iResolution.xy)/iResolution.y;
   vec2 uv = vUv; 
    float battery = 1.0;
    {
        vec3 col = vec3(.8, 0.2, 1.0);
            vec2 sunUV = uv;
            // Sun
            sunUV -=  vec2(0.5, 0.5);
            float sunVal = sun(sunUV, battery);
            // color top
            col = mix(col, vec3(1.2, 1.2, 0.1), sunUV.y * 2.6 + 0.2);
            col = mix(vec3(0.2, 0, 0.4), col, sunVal);
        col = mix(vec3(col.r, col.r, col.r) * 0.5, col, battery * 0.7);
        fragColor = vec4(col,1.0);
    }
}
`;

const fragmentSun = `
//#extension GL_OES_standard_derivatives : enable
//#extension GL_EXT_shader_texture_lod : enable
#ifdef GL_ES
precision mediump float;
#endif
uniform vec3      iResolution;
uniform float     uTime;  
uniform float     iGlobalTime;
uniform float     iChannelTime[4];
uniform vec4      iMouse;
uniform vec4      iDate;
uniform float     iSampleRate;
uniform vec3      iChannelResolution[4];
uniform int       iFrame;
uniform float     iTimeDelta;
uniform float     iFrameRate; 

struct Channel
{
    vec3  resolution;
    float time;
};
uniform Channel iChannel[4];
uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform sampler2D iChannel2;
uniform sampler2D iChannel3;
varying vec2 vUv;


${shadertoyBoilerplate}

void main() {
  mainImage(gl_FragColor, gl_FragCoord.xy);
}
`;

export default fragmentSun;
